﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Data.SqlClient;

namespace WebApplication1.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            List<Cours> lc = new List<Cours>();
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=totoDb;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand("select * from Cours;", con);
             
                con.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lc.Add(new Cours { 
                            CoursId = Convert.ToInt16(reader["CoursId"]),
                            Sigle = reader["Sigle"].ToString(),
                            Titre= reader["Titre"].ToString(),
                            Dsc = reader["dsc"].ToString()
                    });

                }

                ViewBag.mesCours = lc;

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
            }
            finally
            {
                con.Close();

            }

            return View();
        }

       [HttpPost]
        public ActionResult Index(etuduiant e1)
        {

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;Initial Catalog=totoDb;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand("insert into Etudiants(id,nom,email,niveau) values(@i,@n,@e,@d);", con);
                cmd.Parameters.AddWithValue("@i", e1.Id);
                cmd.Parameters.AddWithValue("@n", e1.Nom);
                cmd.Parameters.AddWithValue("@e", e1.Email);
                cmd.Parameters.AddWithValue("@d", e1.Niveau);
                con.Open();

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                string mes = ex.Message;
            }
            finally
            {
                con.Close();
            
            }
            return View();
        }
    }
}