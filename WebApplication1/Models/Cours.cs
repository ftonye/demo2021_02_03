﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Cours
    {
        public int CoursId { get; set; }
        public string Sigle{ get; set; }
        public string Titre{ get; set; }
        public string Dsc { get; set; }

    }
}